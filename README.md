### Node / front-end app for storm project

This app is used to see the data collected by apache storm in a friendlier way. It's created using the vue.js frontend framework.

The backend for this application can be found [here](https://github.com/saibot94/storm-twitter-parser)
